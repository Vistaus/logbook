/*
 * Copyright (C) 2022  Emanuele Sorce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * logbook is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QDir>

#include "example.h"

Example::Example() {}

QString Example::get(const QDate &date)
{
    QString filepath(QString("/home/phablet/.local/share/logbook.emanuelesorce/Diary/") +
                     (QString::number(date.year())) + "/" +
                     (date.month() < 10 ? QString("0") : QString("")) + QString::number(date.month()) + "/" +
                     (date.day() < 10 ? QString("0") : QString("")) + QString::number(date.day()) + ".md");
    QFile file(filepath);
    qDebug() << filepath;
    if (file.exists())
    {
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return "";

        QString content = file.readAll();
        qDebug() << content;
        return content;
    }
    else
    {
        qDebug() << "file not found";
        return "";
    }
}

void Example::set(const QDate &date, const QString &content)
{
    QDir dir("/home/phablet/.local/share/logbook.emanuelesorce/Diary/" +
             (QString::number(date.year())) + "/" +
             (date.month() < 10 ? QString("0") : QString("")) + QString::number(date.month()));
    if (!dir.exists())
        dir.mkpath(dir.absolutePath());

    QString filepath(dir.absolutePath() + '/' + (date.day() < 10 ? QString("0") : QString("")) + QString::number(date.day()) + ".md");
    QFile file(filepath);
    qDebug() << filepath;
    if (file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream out(&file);
        out << content;
    }
    else
    {
        qDebug() << "file not found";
    }
}