# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the logbook.emanuelesorce package.
#
# Heimen Stoffels <vistausss@fastmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: logbook.emanuelesorce\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-20 13:56+0000\n"
"PO-Revision-Date: 2022-04-21 13:27+0200\n"
"Last-Translator: Heimen Stoffels <vistausss@fastmail.com>\n"
"Language-Team: Dutch <>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.03.90\n"

#: ../qml/Main.qml:59
msgid "About"
msgstr "Over"

#: ../qml/Main.qml:102
msgid ""
"LogBook is an open source diary app for Ubuntu Touch.<br>Community is what "
"makes this app possible, so pull requests, translations, feedback and "
"donations are very appreciated <br>This app stands on the shoulder of "
"various Open Source projects, see source code for licensing details"
msgstr ""
"Logboek is een opensource-dagboekapp voor Ubuntu Touch.<br>Deze app komt tot"
" stand "
"door de gemeenschap, dus samenvoegverzoeken (‘merge requests’), vertalingen,"
" feedback en "
"donaties worden zeer gewaardeerd! <br>Deze app is gebouwd met behulp van"
" diverse andere "
"opensourceprojecten. Bekijk voor meer informatie de broncode."

#: ../qml/Main.qml:107
msgid "❤Donate❤"
msgstr "❤Doneren❤"

#: ../qml/Main.qml:115
msgid "See source on:"
msgstr "Broncode:"

#: ../qml/Main.qml:122
msgid "Report bug or feature request"
msgstr "Bug melden of idee delen"

#: ../qml/Main.qml:193
msgid "Yesterday"
msgstr "Gisteren"

#: ../qml/Main.qml:207
msgid "Tomorrow"
msgstr "Morgen"

#: ../qml/Main.qml:258
msgid "Cancel"
msgstr "Annuleren"

#: ../qml/Main.qml:272
msgid "Save"
msgstr "Opslaan"

#: ../qml/Main.qml:285
msgid "Edit"
msgstr "Bewerken"

#: ../qml/Main.qml:302
msgid "No entry for this day"
msgstr "Je hebt op deze dag niets geschreven"

#: logbook.desktop.in.h:1
msgid "LogBook"
msgstr "Logboek"
